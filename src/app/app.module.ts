import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap';
import { AppComponent } from './app.component';
import { BookModalComponent } from './manage-books/book-modal/book-modal.component';
import {HttpModule} from '@angular/http';
import {AppService} from './app.service';
import { ReactiveFormsModule } from '@angular/forms';
import { ManageBooksComponent } from './manage-books/manage-books.component';
import { BooksTableComponent } from './manage-books/books-table/books-table.component';
import { ConfirmationModalComponent } from './manage-books/confirmation-modal/confirmation-modal.component';
import { EachWordFirstUpperRestLower } from './eachWordFirstUpperRestLower.pipe';
import { FilterNoneEnglishLetters } from './filterNoneEnglishLetters.pipe';

@NgModule({
  declarations: [
    AppComponent,
    BookModalComponent,
    ManageBooksComponent,
    BooksTableComponent,
    ConfirmationModalComponent,
    EachWordFirstUpperRestLower,
    FilterNoneEnglishLetters
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    ModalModule.forRoot()
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
