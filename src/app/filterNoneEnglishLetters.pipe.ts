import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'filterNoneEnglishLetters'})
export class FilterNoneEnglishLetters implements PipeTransform {
  transform(value: string):string {
    return value.replace(/[^0-9a-z]/gi, '').replace(/([A-Z])/g, ' $1').trim();
  }
}