import { Component, OnInit } from '@angular/core';
import {AppService, Book} from '../../app.service';

@Component({
  selector: 'books-table',
  templateUrl: './books-table.component.html',
  styleUrls: ['./books-table.component.scss']
})

export class BooksTableComponent {
   guest = "Mickey";
   books : Array<Book> = [];   
   currentUpdateBook : Book = null;
   currentUpdateIndex : number = null;
   openConfirmationModal : boolean = false;
   openModal : boolean = false;

   constructor(private _appService : AppService){
    this._appService.getData()
      .subscribe((res  :Array<Book>) =>{
        this.books = res;
      });
  }


  editBookEvent(book  :Book) : void {
    this.books[this.currentUpdateIndex] = book;
    this.currentUpdateIndex = null;
  }

  createBookEvent(book : Book) : void {   // this is the "new Book" from "book-modal.ts"
    this.books.push(book);
    this.currentUpdateIndex = null;
  }

  deleteBookEvent(bookIndexToDelete : number) : void {
      this.books.splice(bookIndexToDelete,1);
  }

}
