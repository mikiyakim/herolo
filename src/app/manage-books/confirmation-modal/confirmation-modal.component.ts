import { Component, OnInit, ViewChild, Input, Output,EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.scss']
})

export class ConfirmationModalComponent implements OnChanges {
  @ViewChild('confirmModal') public confirmModal: ModalDirective;

  @Input() open: boolean = false;
  @Input() bookIndex : number = null;
  @Output() deleteBookEvent  = new EventEmitter();
  
  ngOnChanges(changes: SimpleChanges) {
    if (!changes['open'].firstChange && changes['open'].previousValue !== changes['open'].currentValue) {
      this.confirmModal.show();
    }
  }

  closeModal(): void {
    this.confirmModal.hide();
  }

  delete() : void {
    this.deleteBookEvent.emit(this.bookIndex); 
    this.closeModal();
  }
}
