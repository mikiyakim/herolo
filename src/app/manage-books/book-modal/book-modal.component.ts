import { Component, OnInit, Input, Output, OnChanges, ViewChild, SimpleChanges, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Book } from '../../app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'book-modal',
  templateUrl: './book-modal.component.html',
  styleUrls: ['./book-modal.component.scss'],
  providers: [FormBuilder]
})
export class BookModalComponent implements OnChanges {
  constructor(private _fb: FormBuilder) {
    this.bookForm = this._fb.group({
      Autuor: [null, Validators.required],
      Date: [null, [Validators.required, Validators.pattern(this.date_regex)]],
      Title: [null, Validators.required]
    });
  }
  @ViewChild('childModal') public childModal: ModalDirective;

  @Input() open: boolean = false;
  @Input() book: Book = null;
  @Output() editBookEvent = new EventEmitter();
  @Output() createBookEvent = new EventEmitter();
  

  bookForm: FormGroup;
  date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;

  closeModal(): void {
    this.childModal.hide();
  }

  createUpdate(): void {             
    let book: Book = new Book(        // the new Book object only created in the form and not on the book-table
      this.bookForm.controls['Autuor'].value,
      this.bookForm.controls['Date'].value,
      this.bookForm.controls['Title'].value
    )
    if (this.book) {       // not null ..which mean we are in edit mode
      this.editBookEvent.emit(book);
    } else {
      this.createBookEvent.emit(book);
    }
   
    this.closeModal();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!changes['open'].firstChange && changes['open'].previousValue !== changes['open'].currentValue) {
      this.bookForm = this._fb.group({
        Autuor: [this.book ? this.book.Autuor : null, Validators.required],
        Date: [this.book ? this.book.Date : null, [Validators.required, Validators.pattern(this.date_regex)]],
        Title: [this.book ? this.book.Title : null, Validators.required]
      });
      console.log( this.bookForm.controls['Date'].errors);
      this.childModal.show();
    }
  }

}
