import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class AppService {
  constructor(public http: Http) { }

  getData(): Observable<Array<Book>> {
    return this.http.get("../assets/books.json")
      .map((res: Response) => res.json());
  }
}

export class Book {
  Autuor: string;
  Date: string;
  Title: string;
  constructor(Autuor: string, Date: string, Title: string) {
    this.Autuor = Autuor;
    this.Date = Date;
    this.Title = Title;
  }
}